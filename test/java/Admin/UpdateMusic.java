package Admin;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import net.minidev.json.JSONObject;

public class UpdateMusic {
	@Test
	  public void updateproduct() {
	      JSONObject req = new JSONObject();
	      req.put("title", "Kaisi ahi ye dooriyan");
	      req.put("album", "kuch baate ankahin si");
	      req.put("artist", "JJ");
	      req.put("genre", "rajni");
	      baseURI="http://localhost:8090/muzix/admin";

	 

	      given()
	      .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJKSjExIiwiZXhwIjoxNjkzNTE2ODc2LCJpYXQiOjE2OTM0ODA4NzZ9.WZK_SoPuGxlQ7vbUzCMjOv5KB9bWFE8jJ3H1FCCiiMQ")

	      .contentType(ContentType.JSON).
	      accept(ContentType.JSON)
	      .body(req.toJSONString())
	      .when()
	      .put("/4")
	      .then()
	      .statusCode(200)
	      .log().all();
	  }
	}