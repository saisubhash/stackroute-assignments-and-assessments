package Admin;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import net.minidev.json.JSONObject;
public class AdminPost {
  @Test
  public void AdminPost() {
      JSONObject req = new JSONObject();
      req.put("musicId", "4");
      req.put("title", "natu natu returns phir se ");
      req.put("genre", "mass rajni AA");
      req.put("album", "RRRRR");
      req.put("artist", "ntrr");
      baseURI="http://localhost:8090/muzix/admin/";
      given().header("Content-Type", "application/json")

      .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJKSjExIiwiZXhwIjoxNjkzNTE2ODc2LCJpYXQiOjE2OTM0ODA4NzZ9.WZK_SoPuGxlQ7vbUzCMjOv5KB9bWFE8jJ3H1FCCiiMQ")

      .body(req.toJSONString()).when().post("/add").then().statusCode(201).log().all();
  }
}