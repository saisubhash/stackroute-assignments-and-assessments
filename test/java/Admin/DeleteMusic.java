package Admin;

import org.testng.annotations.Test;
import io.restassured.http.ContentType;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import org.json.JSONObject;

public class DeleteMusic {
	 @Test
	  public void deleteProductyId() {
	      JSONObject req = new JSONObject();
	      baseURI="http://localhost:8090/muzix/admin";

	 

	      given()
	     .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJKSjExIiwiZXhwIjoxNjkzNTE2ODc2LCJpYXQiOjE2OTM0ODA4NzZ9.WZK_SoPuGxlQ7vbUzCMjOv5KB9bWFE8jJ3H1FCCiiMQ")

	      .contentType(ContentType.JSON).accept(ContentType.JSON)
	      .when()
	      .delete("/3")
	      .then()
	      .statusCode(200)
	      .log().all();
	  }
	}