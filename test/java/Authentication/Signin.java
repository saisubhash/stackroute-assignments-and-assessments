package Authentication;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class Signin {

    @BeforeClass
    public void setUp() {
        baseURI = "http://localhost:8090/user";
    }

    @Test
    public void testSuccessfulSignIn() throws JSONException {
        JSONObject req = new JSONObject();
        req.put("userName", "JJ");
        req.put("password", "JJ1234");

        given()
            .header("Content-Type", "application/json")
            .body(req.toString())
        .when()
            .post("/signin")
        .then()
            .statusCode(200)
            .log().all();
    }
}
