package com.example.demo;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class usercontroller {
//	@GetMapping
//    public String getusers() {
//    	return "http get request was sent";
//   }
//	
//	@GetMapping
//	public int get() {
//		return 3;
//	}
	@GetMapping(path="/{userid}")
	public String getuser(@PathVariable String userid) {
		return "http get request was sent for userid:"+userid;
	}
	
	@GetMapping
	public String getUsers(@RequestParam(value="page") int pageno, @RequestParam(value="limit") int limitno) {
		return "http request was sent with the page:"+pageno+" and with the limit "+limitno;
	}
	
	@PostMapping
	public String postuser() {
		return "http post request was sent";
	}
	
	@PutMapping
	public String putuser() {
		return "http put request was sent";
	}
	
	@DeleteMapping
	public String deleteuser() {
		return "http delete request was sent";
	}
}
