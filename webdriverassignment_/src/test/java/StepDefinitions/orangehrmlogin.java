package StepDefinitions;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class orangehrmlogin {
	
	WebDriver driver;
	@Given("launch the browser")
	public void launch_the_browser() {
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
//	    throw new io.cucumber.java.PendingException();
	}
	
	@And("Navigate to the URL")
	public void navigate_to_the_url() {
	  
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
//		throw new io.cucumber.java.PendingException();
	}
	
	@When("enter the username and password")
	public void enter_the_username_and_password() {
	    
		 driver.findElement(By.name("username")).sendKeys("Admin");
		 driver.findElement(By.name("password")).sendKeys("admi123");
//	    throw new io.cucumber.java.PendingException();
	}
	@And("clickon submit button")
	public void clickon_submit_button() {
	   
		 driver.findElement(By.xpath("//button[@type='submit']")).click();
//	    throw new io.cucumber.java.PendingException();
	}
	@Then("Home page should be displayed")
	public void home_page_should_be_displayed() {
	try {
		 Boolean searchdispalyed=driver.findElement(By.xpath("//input[@placeholder='Search']")).isDisplayed();

		 if (searchdispalyed.equals(true))
		 {
			 System.out.println("Login Successfull");
		 }
		 else
		 {
			 System.out.println("Login unsuccessfull");
		 }
	}
	catch(Exception e) {
		System.out.println("Login unsuccessfull");
	}
//		 throw new io.cucumber.java.PendingException();
	}
	@And("close the browser")
	public void close_the_browser() {
	    // Write code here that turns the phrase above into concrete actions
		driver.quit();
//	    throw new io.cucumber.java.PendingException();
	}
}