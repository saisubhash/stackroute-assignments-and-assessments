package webdriverassignment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class orangehrm_pagefactory {
//	By ohrmuser=By.name("username");
	@FindBy(name="username")
	WebElement ohrmuser;
	
//	By ohrmpass=By.name("password");
	@FindBy(name="password")
	WebElement ohrmpass;
	
//	By ohrmsubmit=By.xpath("//button[@type='submit']");
	@FindBy(xpath ="//button[@type='submit']" )
	WebElement ohrmsubmit;
	
//	By ohrmsearch=By.xpath("//input[@placeholder='Search']");
	@FindBy(xpath = "//input[@placeholder='Search']")
	WebElement ohrmsearch;
	
	public void enteruser(String user) {
		ohrmuser.sendKeys(user);
	}
	public void enterpass(String pass) {
		ohrmpass.sendKeys(pass);
	}
	public void loginclick() {
		ohrmsubmit.click();
	}
	public boolean searchdisplay() {
		Boolean search=ohrmsearch.isDisplayed();
		return search;
	}

}
