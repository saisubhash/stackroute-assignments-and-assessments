package webdriverassignment;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.github.dockerjava.api.model.Driver;


import io.github.bonigarcia.wdm.WebDriverManager;

@Test
public class godaddy {
	
	WebDriver driver;
	
	@BeforeTest
	public void browseropen() {
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.get("https://www.godaddy.com/");
		driver.manage().window().maximize();
	}
	
  @Test(priority = 1)
  void title() {
	  String actualtitle=driver.getTitle();
	  System.out.println(actualtitle);
	  Assert.assertEquals("Domain Names, Websites, Hosting & Online Marketing Tools - GoDaddy IN", actualtitle);
  }
  
  @Test(priority = 2)
  void url() {
	  String currenturl=driver.getCurrentUrl();
	  System.out.println(currenturl);
	  Assert.assertEquals("https://www.godaddy.com/en-in", currenturl);
  }
  
  @Test(priority = 3)
  void pagesource() {
	  String pagesource=driver.getPageSource();
	  Assert.assertTrue(pagesource.contains("Domain Names, Websites, Hosting & Online Marketing Tools - GoDaddy IN"));    
	  }

  @AfterTest
   void quit() {
	  driver.quit();
  }
}
