package webdriverassignment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class orangehrm_pageobjects {
	WebDriver driver;
	By ohrmuser=By.name("username");
	By ohrmpass=By.name("password");
	By ohrmsubmit=By.xpath("//button[@type='submit']");
	By ohrmsearch=By.xpath("//input[@placeholder='Search']");
	
	public orangehrm_pageobjects(WebDriver driver2) {
		// TODO Auto-generated constructor stub
		this.driver=driver2;
	}
	public void enteruser(String user) {
		driver.findElement(ohrmuser).sendKeys(user);
	}
	public void enterpass(String pass) {
		driver.findElement(ohrmpass).sendKeys(pass);
	}
	public void loginclick() {
		driver.findElement(ohrmsubmit).click();
	}
	public boolean searchdisplay() {
		Boolean search=driver.findElement(ohrmsearch).isDisplayed();
		return search;
	}

}
