package webdriverassignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import io.github.bonigarcia.wdm.WebDriverManager;

public class datadriven_pagefactory {
	WebDriver driver;
	ExtentReports extent;
	ExtentSparkReporter spark;
	ExtentTest test;
	
  @Test(dataProvider = "dp")
  public void login(String username, String password,String screenshot) throws InterruptedException {
	  
	  driver.get("https://opensource-demo.orangehrmlive.com");
	  
//	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input")).sendKeys(username);
//	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[2]/div/div[2]/input")).sendKeys(password);
//	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button")).click();
//	  Boolean search=driver.findElement(By.xpath("//input[@placeholder='Search']")).isDisplayed();
	  
//	  ExtentReports extent=new ExtentReports();
//	  ExtentSparkReporter spark=new ExtentSparkReporter("./Screenshots/datadriven_pf.html");
//	  extent.attachReporter(spark);
	  
	  test=extent.createTest("Launch Browser, login orangehrm and verify the home page is displayed");
	  
	orangehrm_pagefactory obj1=PageFactory.initElements(driver, orangehrm_pagefactory.class);
	  obj1.enteruser(username);
	  obj1.enterpass(password);
	  obj1.loginclick();
	  Thread.sleep(3000);
	  TakesScreenshot ts=(TakesScreenshot)driver;
	  File sourcefile=ts.getScreenshotAs(OutputType.FILE);
	  File destfile=new File("./Screenshots/"+screenshot);
	  try {
		  FileUtils.copyFile(sourcefile, destfile);
	  }
	  catch(Exception e) {
		  e.printStackTrace();
	  }
	  
	  Boolean search=obj1.searchdisplay();
	  
	  Assert.assertEquals(search,true);
	  if(search.equals(true)) {
		  test.addScreenCaptureFromPath("E:\\ECLIPSE WORKSPACE\\selenium-webdriver-i\\webdriverassignment\\Screenshots\\s1.png");
		  test.pass("Login success");
		  System.out.println("login success");
	  }
	  else {
		  System.out.println("login fail");
		  test.fail("login fail");
	  }
//	  extent.flush();
  }
  @BeforeMethod
  public void beforeMethod() {
	  WebDriverManager.chromedriver().setup();
	  driver=new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
  }
  @AfterMethod
  public void afterMethod() {
	  driver.quit();
	  extent.flush();
  }
  
  @BeforeSuite
  public void reports() {
	  extent=new ExtentReports();
	  spark=new ExtentSparkReporter("./Screenshots/datadriven_pf.html");
	  extent.attachReporter(spark);
	  
  }
  
  @DataProvider(name="dp")
  public String[][] prop() throws IOException {
String[][] data=new String[4][3];
	  
	  File file=new File("E:\\ECLIPSE WORKSPACE\\selenium-webdriver-i\\webdriverassignment\\inputdata.xlsx");
	  FileInputStream fis=new FileInputStream(file);
	  XSSFWorkbook workbook=new XSSFWorkbook(fis);
	  XSSFSheet sheet=workbook.getSheetAt(0);
	  int rowcount=sheet.getPhysicalNumberOfRows();
	  System.out.println("row count:"+rowcount);
	  
	  for(int i=0;i<rowcount;i++)
	  {
		 
		  data[i][0]=sheet.getRow(i).getCell(0).getStringCellValue();
		 
		  data[i][1]=sheet.getRow(i).getCell(1).getStringCellValue();
		  
		  data[i][2]=sheet.getRow(i).getCell(2).getStringCellValue();
	  }
	    
	 
	  return data;
  }
}
