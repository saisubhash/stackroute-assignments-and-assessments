package webdriverassignment;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Datadriven_properties {
	WebDriver driver;
  @Test(dataProvider = "dp")
  public void login(String username, String password) {
	  driver.get("https://opensource-demo.orangehrmlive.com");
//	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input")).sendKeys(username);
//	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[2]/div/div[2]/input")).sendKeys(password);
//	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button")).click();
	  Boolean search=driver.findElement(By.xpath("//input[@placeholder='Search']")).isDisplayed();
	  Assert.assertEquals(search,true);
	  if(search.equals(true)) {
		  System.out.println("login success");
	  }
	  else {
		  System.out.println("login fail");
	  }
  }
  @BeforeMethod
  public void beforeMethod() {
	  WebDriverManager.chromedriver().setup();
	  driver=new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
  }
  @AfterMethod
  public void afterMethod() {
	  driver.quit();
  }
  
  @DataProvider(name="dp")
  public String[][] prop() throws IOException {
	  String[][] data=new String[1][2];
	  InputStream input=new FileInputStream("E:\\ECLIPSE WORKSPACE\\selenium-webdriver-i\\webdriverassignment\\logindata.properties");
	  Properties prob=new Properties();
	  prob.load(input);
	   data[0][0]=prob.getProperty("username");
	   data[0][1]=prob.getProperty("password");
	  
	  return data;
  }
}
