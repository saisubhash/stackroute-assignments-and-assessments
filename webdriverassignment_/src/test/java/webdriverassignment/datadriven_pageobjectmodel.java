package webdriverassignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class datadriven_pageobjectmodel {
	WebDriver driver;
  @Test(dataProvider = "dp")
  public void login(String username, String password) {
	  
	  driver.get("https://opensource-demo.orangehrmlive.com");
	  
//	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input")).sendKeys(username);
//	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[2]/div/div[2]/input")).sendKeys(password);
//	  driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button")).click();
//	  Boolean search=driver.findElement(By.xpath("//input[@placeholder='Search']")).isDisplayed();
	  
	  orangehrm_pageobjects obj1=new orangehrm_pageobjects(driver);
	  obj1.enteruser(username);
	  obj1.enterpass(password);
	  obj1.loginclick();
	  Boolean search=obj1.searchdisplay();
	  
	  Assert.assertEquals(search,true);
	  if(search.equals(true)) {
		  System.out.println("login success");
	  }
	  else {
		  System.out.println("login fail");
	  }
  }
  @BeforeMethod
  public void beforeMethod() {
	  WebDriverManager.chromedriver().setup();
	  driver=new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
  }
  @AfterMethod
  public void afterMethod() {
	  driver.quit();
  }
  
  @DataProvider(name="dp")
  public String[][] prop() throws IOException {
String[][] data=new String[4][2];
	  
	  File file=new File("E:\\ECLIPSE WORKSPACE\\selenium-webdriver-i\\webdriverassignment\\inputdata.xlsx");
	  FileInputStream fis=new FileInputStream(file);
	  XSSFWorkbook workbook=new XSSFWorkbook(fis);
	  XSSFSheet sheet=workbook.getSheetAt(0);
	  int rowcount=sheet.getPhysicalNumberOfRows();
	  System.out.println("row count:"+rowcount);
	  
	  for(int i=0;i<rowcount;i++)
	  {
		 
		  data[i][0]=sheet.getRow(i).getCell(0).getStringCellValue();
		 
		  data[i][1]=sheet.getRow(i).getCell(1).getStringCellValue();
	  }
	    
	 
	  return data;
  }
}
