package webdriverassignment;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class poi_1 {
	WebDriver driver;
	
  @BeforeClass
  public void setup() {
	  WebDriverManager.chromedriver().setup();
	  driver=new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
  }
  
  @Test(dataProvider = "dp1")
  public void logintest(String txt1,String txt2,String txt3 ) {
	  driver.get("https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F");
	  WebElement email=driver.findElement(By.id("Email"));
	  email.clear();
	  email.sendKeys(txt1);
	  
	  WebElement pass=driver.findElement(By.id("Password"));
	  pass.clear();
	  pass.sendKeys(txt2);
	 
	  driver.findElement(By.xpath("//button[contains(text(),'Log in')]")).click(); //Login button
	  
	  String exp_title="Dashboard / nopCommerce administration";
	  String act_title=driver.getTitle();
	  
	  if(txt3.equals("valid"))
		{
			if(exp_title.equals(act_title))
			{
				driver.findElement(By.linkText("Logout")).click();
				Assert.assertTrue(true);
			}
			else
			{
				Assert.assertTrue(false);
			}
		}
		else if(txt3.equals("invalid"))
		{
			if(exp_title.equals(act_title))
			{
				driver.findElement(By.linkText("Logout")).click();
				Assert.assertTrue(false);
			}
			else
			{
				Assert.assertTrue(true);
			}
		}
		
	}
  
  @DataProvider(name="dp1")
  public String[][] data1(){
	  String data[][] = {
			  {"admin@yourstore.com","adm","invalid"},
			  {"admin@youstore.com","admin","invalid"},
			  {"admin@ourstore.com","adm","invalid"},
			  {"admin@yourstore.com","admin","valid"}
	  };
	  
	  
	  
	  return data;
  }
  
  @AfterClass
  public void close() {
	  driver.quit();
  }
}
