package webdriverassignment;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class techlistic {
	WebDriver driver;
  @Test
  public void demoform() throws InterruptedException {
	  driver.findElement(By.name("firstname")).sendKeys("Anonymous");
	  Thread.sleep(1000);
	  driver.findElement(By.name("lastname")).sendKeys("Ghost");
	  Thread.sleep(1000);
	  
	  JavascriptExecutor js=(JavascriptExecutor)driver;
	  js.executeScript("window.scrollBy(0,300)");
	  
	  driver.findElement(By.id("sex-0")).click();
	  Thread.sleep(1000);
	  driver.findElement(By.id("exp-1")).click();
	  Thread.sleep(1000);
	  driver.findElement(By.id("datepicker")).sendKeys("20-08-2023");
	  Thread.sleep(1000);
	  js.executeScript("window.scrollBy(0,300)");
	  Thread.sleep(2000);
	  driver.findElement(By.id("profession-1")).click();
	  Thread.sleep(1000);
	  driver.findElement(By.id("tool-0")).click();
	  driver.findElement(By.id("tool-2")).click();
	  Thread.sleep(1000);
	  
	  WebElement e=driver.findElement(By.id("continents"));
	  e.click();
	  Thread.sleep(1000);
	  Select s=new Select(e);
	  s.selectByVisibleText("Antartica");
	  Thread.sleep(1000);
	  
	  WebElement e1=driver.findElement(By.id("selenium_commands"));
	  e1.click();
	  Thread.sleep(1000);
	  Select s1=new Select(e1);
	  s1.selectByVisibleText("Navigation Commands");
	  s1.selectByVisibleText("WebElement Commands");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.id("photo")).sendKeys("C:\\Users\\Wel Come\\Pictures\\Screenshots\\Screenshot (13).png");
	  Thread.sleep(2000);
	  driver.findElement(By.linkText("Click here to Download File")).click();
	  Thread.sleep(1000);
	  driver.navigate().back();
	  Thread.sleep(1000);
	  driver.findElement(By.id("submit")).click();
	  Thread.sleep(2000);
  }
  @BeforeTest
  public void beforeTest() {
	  WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.techlistic.com/p/selenium-practice-form.html");
//		driver.findElement(By.id("ezmob-footer-close")).click();
  }

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
