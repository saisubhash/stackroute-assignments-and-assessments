package com.stackroute.albumapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.albumapp.model.Album;
import com.stackroute.albumapp.service.AlbumService;

@RestController
@RequestMapping("/api/v1/album")
public class AlbumController {

		@Autowired
		private AlbumService albumService;

		

		@PostMapping
		public ResponseEntity<?> addAlbum(@RequestBody Album album) {
			
			System.out.println(album);
			Album alb = albumService.addAlbum(album);
			if( alb !=null) {
				return new ResponseEntity<Album>(alb,HttpStatus.CREATED);
			}
			
			return new ResponseEntity<String>("Album not added",HttpStatus.CONFLICT);

		}
		
		@GetMapping
		public ResponseEntity<?> getAllAlbums() {
			
			List<Album> albumlist = albumService.getAllAlbums();
			
			if(albumlist.size() > 0) {
				return new ResponseEntity<List<Album>>(albumlist,HttpStatus.OK);
			}

			return new ResponseEntity<String>("List is empty",HttpStatus.OK);
			
		}
			
		@PutMapping
		public ResponseEntity<?> updateAlbum(@RequestBody Album album) {
			
			Album updatedalb = albumService.updateAlbum(album);
			if( updatedalb !=null) {
				return new ResponseEntity<Album>(updatedalb,HttpStatus.OK);
			}
			
			return new ResponseEntity<String>("Album not updated",HttpStatus.CONFLICT);

		}
			

		@DeleteMapping("/{albumid}")
		public ResponseEntity<?> deleteAlbum(@PathVariable String albumid) {
			
			Album deletedalb = albumService.deleteAlbum(albumid);;
			if( deletedalb !=null) {
				return new ResponseEntity<Album>(deletedalb,HttpStatus.OK);
			}
			
			return new ResponseEntity<String>("Album not Deleted",HttpStatus.CONFLICT);
			

		}
				
		
		@GetMapping("/{albumid}")
		public ResponseEntity<?> getAlbumById(@PathVariable String albumid ) {
			
			Album album = albumService.getAlbumByAlbumId(albumid);
			
			if(album != null) {
				return new ResponseEntity<Album>(album,HttpStatus.OK);
			}

			return new ResponseEntity<String>("Album not found",HttpStatus.OK);
			
		}
		
		@GetMapping("/singer/{singername}")
		public ResponseEntity<?> getAllAlbumsBySingername(@PathVariable String singername) {
			
			List<Album> albumlist = albumService.getAllAlbumsBySinger(singername);
			
			if(albumlist.size() > 0) {
				return new ResponseEntity<List<Album>>(albumlist,HttpStatus.OK);
			}

			return new ResponseEntity<String>("No album list for the Singer",HttpStatus.OK);
			
		}

}

