package com.stackroute.albumapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stackroute.albumapp.model.Album;

@Repository
public interface AlbumRepository extends JpaRepository<Album, String> {
	
	List<Album> findBySinger(String singername);
//	List<Album> findBySingerAndLanguage(String singername,String language);
	
}
