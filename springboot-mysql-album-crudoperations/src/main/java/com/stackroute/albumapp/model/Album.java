package com.stackroute.albumapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Album {

	@Id
	@Column(length = 20)
	private String albumid;
	@Column(length = 100)
	private String albumname;
	@Column(length = 50)
	private String singer;
	@Column(length = 20)
	private double price;
	@Column(length = 50)
	private String language;
	
	public Album() {
		super();
	}

	public Album(String albumid, String albumname, String singer, double price, String language) {
		super();
		this.albumid = albumid;
		this.albumname = albumname;
		this.singer = singer;
		this.price = price;
		this.language = language;
	}
	
	public String getAlbumid() {
		return albumid;
	}

	public void setAlbumid(String albumid) {
		this.albumid = albumid;
	}

	public String getAlbumname() {
		return albumname;
	}

	public void setAlbumname(String albumname) {
		this.albumname = albumname;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public String toString() {
		return "Album [albumid=" + albumid + ", albumname=" + albumname + ", singer=" + singer + ", price=" + price
				+ ", language=" + language + "]";
	}

	
	
	
	
}
