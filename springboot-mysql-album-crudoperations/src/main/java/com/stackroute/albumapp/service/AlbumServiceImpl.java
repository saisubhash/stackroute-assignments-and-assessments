package com.stackroute.albumapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.albumapp.model.Album;
import com.stackroute.albumapp.repository.AlbumRepository;

@Service
public class AlbumServiceImpl implements AlbumService{

	@Autowired
	private AlbumRepository albumRepository;
	
	public Album addAlbum(Album album) {
		Album alb = albumRepository.save(album);
		return alb;
	}

	public List<Album> getAllAlbums() {
		return albumRepository.findAll();
	}

	public Album updateAlbum(Album album) {
		if(albumRepository.existsById(album.getAlbumid())) {
			albumRepository.save(album);
			return album;
		}
		return null;

	}

	public Album deleteAlbum(String albumid) {
		
		if(albumRepository.existsById(albumid)) {
			Album album = albumRepository.findById(albumid).get();
			albumRepository.deleteById(albumid);
			return album;
		}
		return null;

	}

	@Override
	public Album getAlbumByAlbumId(String albumid) {
		
		Optional<Album> albumdata = albumRepository.findById(albumid);
		if(albumdata.isPresent())
			return albumdata.get();
		else
			return null;
	}

	@Override
	public List<Album> getAllAlbumsBySinger(String singername) {
		return albumRepository.findBySinger(singername);
	}

	

}
