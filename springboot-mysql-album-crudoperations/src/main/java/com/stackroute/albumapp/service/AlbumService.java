package com.stackroute.albumapp.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.stackroute.albumapp.model.Album;


public interface AlbumService {

	public Album addAlbum(Album album);
	public List<Album> getAllAlbums();
	public Album updateAlbum(Album album);
	public Album deleteAlbum(String albumid);
	public Album getAlbumByAlbumId(String albumid);
	public List<Album> getAllAlbumsBySinger(String singername);
	
	
}
