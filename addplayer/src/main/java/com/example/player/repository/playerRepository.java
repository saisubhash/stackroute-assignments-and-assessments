package com.example.player.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*; 
import org.springframework.stereotype.Repository;

import com.example.player.model.Player;

import org.springframework.*;
import org.springframework.context.annotation.Bean;

@Repository
public interface playerRepository extends JpaRepository<Player, String>{
  
}
