package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import io.github.bonigarcia.wdm.WebDriverManager;
import PageObject.AmazonLanguage;

public class LanguageChange {
    private WebDriver driver;
    private AmazonLanguage amazonHomePage;

    @BeforeMethod
    public void beforeMethod() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        amazonHomePage = new AmazonLanguage(driver);
    }

    @Test
    public void changeCountry() throws IOException {
        String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String reportname = "LanguageChange-" + timestamp + ".html";
        String screenshot = "LanguageChange" + timestamp + ".png";
        ExtentReports extent = new ExtentReports();
        ExtentSparkReporter spark = new ExtentSparkReporter(reportname);
        extent.attachReporter(spark);
        ExtentTest test = extent.createTest("Lauch Browser");

        amazonHomePage.navigateToHomePage("https://www.amazon.in/");
        amazonHomePage.clickLanguagePreference();
        amazonHomePage.selectLanguage("hi_IN");
        amazonHomePage.saveLanguagePreference();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        String updatedLang = amazonHomePage.getCurrentLanguage();
        System.out.println(updatedLang);
        
        // Rest of your test logic remains the same.

        extent.flush();
    }

    @AfterMethod
    public void afterMethod() {
        driver.quit();
    }

}