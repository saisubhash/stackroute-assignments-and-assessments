package AmazonTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AmazonProductPage {
    private WebDriver driver;

    public AmazonProductPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnProduct() {
        driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[1]/div[1]/div[3]/div/div/div/div/div/div/div/div[2]/div/div/div[1]/h2/a/span")).click();
    }

    public void addToCart() {
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-button\"]")).click();
    }

    public void goToCart() {
        driver.findElement(By.xpath("//input[@aria-labelledby=\"attach-sidesheet-view-cart-button-announce\"]")).click();
    }

    public boolean isAddedToCart() {
        return driver.findElement(By.xpath("//span[@class=\"a-truncate-cut\"]")).isDisplayed();
    }
}
