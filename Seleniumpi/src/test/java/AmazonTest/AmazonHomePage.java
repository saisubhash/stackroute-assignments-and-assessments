package AmazonTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AmazonHomePage {
    private WebDriver driver;

    public AmazonHomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void openAmazonHomePage(String url) {
        driver.get(url);
    }

    public void searchForProduct(String searchTerm) {
        driver.findElement(By.xpath("//input[@placeholder=\"Search Amazon.in\"]")).sendKeys(searchTerm);
        driver.findElement(By.xpath("//input[@id=\"nav-search-submit-button\"]")).click();
    }
}
