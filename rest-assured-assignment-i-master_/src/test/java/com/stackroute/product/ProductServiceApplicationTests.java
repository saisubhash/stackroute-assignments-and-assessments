package com.stackroute.product;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProductServiceApplicationTests {

	 @Test
	  public void get_users() {
		  baseURI = "http://localhost:9102/productservice";
		  given().get("/products").then().statusCode(200).log().all();
	  }

}
