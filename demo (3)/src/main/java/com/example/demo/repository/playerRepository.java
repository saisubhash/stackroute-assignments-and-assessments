package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*; 
import org.springframework.stereotype.Repository;

import com.example.demo.model.Player;

import org.springframework.*;


@Repository
public interface playerRepository extends JpaRepository<Player, String>{
  
}