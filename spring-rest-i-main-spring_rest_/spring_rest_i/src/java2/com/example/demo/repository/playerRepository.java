package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.player;

@Repository
public interface playerRepository extends JpaRepository<player,Integer>{

	
}
