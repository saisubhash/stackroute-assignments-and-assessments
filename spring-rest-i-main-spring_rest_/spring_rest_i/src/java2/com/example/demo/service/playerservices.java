package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.player;
import com.example.demo.repository.playerRepository;

@Service
public class playerservices implements playerinterface {
	
	@Autowired
	private playerRepository pr;
	
	@Override
	public player add(player p1) {
		// TODO Auto-generated method stub
		return pr.save(p1);
	}

	@Override
	public List<player> getall() {
		// TODO Auto-generated method stub
		return pr.findAll();
	}

	@Override
	public player update(player p1) {
		// TODO Auto-generated method stub
		return pr.save(p1);
	}

	@Override
	public player delete(int id) {
		// TODO Auto-generated method stub
		player p=pr.findById(id).get();
		if(p!=null) {
		pr.deleteById(id);
		return p;
	}
		else {
			return null;
		}
	}

	@Override
	public player getbyid(int id) {
		// TODO Auto-generated method stub
		player p=pr.findById(id).get();
		if(p!=null) {
			return pr.getById(id);
		}
		return null;
	}

}
