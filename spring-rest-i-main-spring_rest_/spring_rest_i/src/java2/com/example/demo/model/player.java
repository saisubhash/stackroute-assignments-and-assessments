package com.example.demo.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="table_")
public class player {
	@Id
	private int playerId;
	private String playerName;
	private String sports;
	private String country;
	
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public String getSports() {
		return sports;
	}
	public void setSports(String sports) {
		this.sports = sports;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public player(int playerId, String playerName, String sports, String country) {
		super();
		this.playerId = playerId;
		this.playerName = playerName;
		this.sports = sports;
		this.country = country;
	}
	public player() {
		super();
	}
	
	
}
