package com.example.demo.service;

import java.util.*;

import com.example.demo.model.player;

public interface playerinterface {
	
	public player add(player p1);
	public List<player> getall();
	public player update(player p1);
	public player delete(int id);
	public player getbyid(int id);
	
}
