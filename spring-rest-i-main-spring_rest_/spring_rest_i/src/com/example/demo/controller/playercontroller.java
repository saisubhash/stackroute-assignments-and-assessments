package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.player;
import com.example.demo.service.playerservices;

@RestController
public class playercontroller {
   @Autowired
   private playerservices ps;
   
   @GetMapping("/getall")
   public List<player> getall(){
	   return ps.getall();
   }
   
   @PostMapping("/add")
   public player add(@RequestBody player p1) {
	   return ps.add(p1);
   }
   
   @PostMapping("/update")
   public player update(@RequestBody player p1) {
	   return ps.update(p1);
   }
   
   @DeleteMapping("/del/{id}")
   public player delete(@PathVariable("id") int pid) {
	   return ps.delete(pid);
   }
   
   @GetMapping("/get/{id}")
   public player getbyid(@PathVariable("id") int pid) {
	   return ps.getbyid(pid);
   }
   
}
