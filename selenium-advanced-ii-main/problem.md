#### Selenium Assignment

##### Automate the following functionalities:

    URL: https://opensource-demo.orangehrmlive.com

    1. Login
        - blank username
        - blank password
        - invalid username
        - invalid password
        - valid username and password

    2. Logout

    3. Add employee
    (https://opensource-demo.orangehrmlive.com/index.php/pim/addEmployee)

    Note:
        - Only advanced xpath and css locators should be used to identify elements
        - Explicit wait should be used for the logout functionality
        - Use testng.xml to run the tests
        - Capture screenshot for the failed cases

