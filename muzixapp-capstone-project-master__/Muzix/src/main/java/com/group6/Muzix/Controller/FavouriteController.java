package com.group6.Muzix.Controller;

import com.group6.Muzix.Beans.Favourite;
import com.group6.Muzix.Services.FavouriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/muzix")
public class FavouriteController {

    @Autowired
    private FavouriteService favouriteService;

    @PostMapping("/fav")
    public ResponseEntity<Favourite> addfavourite(@RequestBody Favourite f){
        Favourite f1 = favouriteService.addfavourite(f);
        return new ResponseEntity<Favourite>(f1, HttpStatus.CREATED);
    }
    @GetMapping("/fav")
    public ResponseEntity<List<Favourite>>getAll(){
        List<Favourite> f2 = favouriteService.getAll();
        return new ResponseEntity<List<Favourite>>(f2, HttpStatus.OK);

    }
    @DeleteMapping("/fav/{music_id}")
    public ResponseEntity<?>DeleteById(@PathVariable int music_id){
        Boolean status = favouriteService.deletefav(music_id);
        return new ResponseEntity<>("deleted successfully", HttpStatus.OK);



    }
}
