package com.group6.Muzix.Services;

import com.group6.Muzix.Beans.Favourite;
import com.group6.Muzix.Repository.FavouriteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavouriteService {
    @Autowired
    private FavouriteRepository favouriteRepository;

    public Favourite addfavourite(Favourite f){
        Favourite favouriteNew=  favouriteRepository.save(f);
        return favouriteNew;
    }
    public List<Favourite> getAll(){
        List<Favourite> favourites = favouriteRepository.findAll();
        return favourites;
    }
    public boolean deletefav(int musicId){
        favouriteRepository.deleteById(musicId);
        return true;
    }
}
