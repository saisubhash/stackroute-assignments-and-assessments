package com.group6.Muzix.Services;

import com.group6.Muzix.Beans.Music;
import com.group6.Muzix.Repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class AdminService {

    @Autowired
    private MusicRepository musicRepository;

    public ResponseEntity<?> getMusicById(Integer id) {
        Music music1 = musicRepository.findOneByMusicId(id);
        return ResponseEntity.ok(music1);
    }

    public ResponseEntity<?> AddnewMusic(Music music) {
        Music musicNew = musicRepository.save(music);
        return new ResponseEntity<>(musicNew,HttpStatus.CREATED);
    }

    public ResponseEntity<?> deleteMusic(Integer id) {
        if(Objects.nonNull(musicRepository.findOneByMusicId(id))){
            musicRepository.deleteById(id);
            return ResponseEntity.ok("Music deleted succesfully");
        }else{
            return new ResponseEntity<>("Music not found", HttpStatus.BAD_REQUEST);        }
    }
}
