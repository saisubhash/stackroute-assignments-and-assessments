package com.group6.Muzix.Controller;

import com.group6.Muzix.Beans.Music;
import com.group6.Muzix.Services.MusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/muzix")
public class MusicController {

    @Autowired
    private MusicService musicService;

    @GetMapping("/all")
    public ResponseEntity<?> getAllmusicforUser(){
        List<Music> musiclist = musicService.getAllmusicforUser();
        if(musiclist.size()>0){
            return new ResponseEntity<>(musiclist, HttpStatus.OK);
        }
        return new ResponseEntity<>("No music available", HttpStatus.CONFLICT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMusicById(@PathVariable("id") Integer id){
        Music msc = musicService.getMusicByid(id);
        if(msc!=null) {
            return new ResponseEntity<>(msc,HttpStatus.OK);
        }
        return new ResponseEntity<String>("Music not found", HttpStatus.CONFLICT);
    }

    @GetMapping("/{album_name}")
    public ResponseEntity<?> getmusicbyalbum(@PathVariable("album_name") String album_name){
        List<Music> albummusic= musicService.getMusicByAlbum(album_name);
        if(albummusic.size()>0){
            return new ResponseEntity<List<Music>>(albummusic, HttpStatus.OK);
        }
        return new ResponseEntity<String>(" Album not found", HttpStatus.CONFLICT);
    }

    @GetMapping("/{genre_name}")
    public ResponseEntity<?> getmusicbygenre(@PathVariable("genre_name") String genre_name){
        List<Music> genremusic= musicService.searchMusicByGenre(genre_name);
        if(genremusic.size()>0){
            return new ResponseEntity<List<Music>>(genremusic, HttpStatus.OK);
        }
        return new ResponseEntity<String>(" Album not found", HttpStatus.CONFLICT);
    }

    @GetMapping("/{artist_name}")
    public ResponseEntity<?> getmusicbyartits(@PathVariable("genre_name") String artist_name){
        List<Music> artistmusic= musicService.getMusicByArtist(artist_name);

        if(artistmusic.size()>0){
            return new ResponseEntity<List<Music>>(artistmusic, HttpStatus.OK);
        }
        return new ResponseEntity<String>(" Artist not found", HttpStatus.CONFLICT);
    }

}
