package com.group6.Muzix.Services;

import com.group6.Muzix.Beans.Music;
import com.group6.Muzix.Repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MusicService {
    @Autowired
    private MusicRepository musicRepository;

    public List<Music> getAllmusicforUser() {
        return musicRepository.findAll();
    }

    public List<Music> getMusicByAlbum(String albumName) {
        return musicRepository.findAllByAlbum(albumName);
    }

    public List<Music> searchMusicByGenre(String genreName) {
        return musicRepository.findAllByGenre(genreName);
    }

    public List<Music> getMusicByArtist(String artistName) {
        return musicRepository.findAllByArtist(artistName);
    }

    public Music getMusicByid(Integer id) {
        return musicRepository.findOneByMusicId(id);
    }
}
