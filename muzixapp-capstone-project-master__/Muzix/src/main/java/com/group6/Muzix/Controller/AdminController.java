package com.group6.Muzix.Controller;

import com.group6.Muzix.Beans.Music;
import com.group6.Muzix.Services.AdminService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/muzix")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping
    public String sayhello(){
        return "You api is passed";
    }


    @GetMapping("admin/{id}")
    public ResponseEntity<?> getMusicbyId(@PathVariable("id") Integer id){
        return adminService.getMusicById(id);
    }

    @PostMapping("admin/add")
    public ResponseEntity<?> AddnewMusic(@RequestBody Music music){
        return adminService.AddnewMusic(music);
    }

    @DeleteMapping("admin/{musicid}")
    public ResponseEntity<?> deleteMusicById(@PathVariable("musicid") Integer id){
        return adminService.deleteMusic(id);
    }

}
