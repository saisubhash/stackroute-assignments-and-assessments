package com.group6.Muzix.Repository;

import com.group6.Muzix.Beans.Favourite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FavouriteRepository extends JpaRepository<Favourite,Integer> {
}
